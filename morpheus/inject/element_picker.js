/**
 * ElementPicker is in charge of picking the right elements across a variety of sites.
 * */

const ElementPicker = (function() {
	var ElementPicker = {
		BindInputs: [],
		BindInputReplace: true,
		CustomCSS: "",
		ModuleLoaded: false,
		MessageFeeds: [],
		MessageElement: "",
		MessageText: "",
		RecipientHints: [],
		Resetters: [],
		options: {
			encryptInputDatas: [],
			ensureElements: [],
			fireFocusEvent: true,
			fireClickEvent: true,
			fireInputTypeEvent: true,
			fireKeyEnterEvent: true,
			fireChangeEvent: true,
		},
		EnsureElements: function() {
			if (!this.options.ensureElements) return;

			// This only works with direct children, e.g. p > span.
			for (let g = 0, gg = this.options.ensureElements.length; g < gg; g++) {
				Element.EnsureExistence(this.options.ensureElements[g]);
			}
		},
		Fetch: function(queries) {
			function fetchOne(q) {
				if (typeof(q) === "string") {
					return document.querySelector(q);
				}
				return q;
			}

			function fetchAll(query) {
				if (typeof(query) === "string") {
					return document.querySelectorAll(query);
				}
				var elements = [];
				Array.prototype.push.apply(elements, query);
				return elements;
			}

			var elements = fetchAll(queries);
			for (var g = 0, gg = elements.length; g < gg; g++) {
				elements[g] = fetchOne(elements[g]);
			}
			return elements;
		},
		FetchBindInputs: function() {
			return this.Fetch(this.BindInputs);
		},
		FetchMessageFeeds: function() {
			return this.Fetch(this.MessageFeeds);
		},
		FetchRecipientHints: function() {
			return this.Fetch(this.RecipientHints);
		},
		FireBindInputEvents: function($e) {
			if (this.options.fireFocusEvent) Element.FireFocusEvent($e);
			if (this.options.fireClickEvent) Element.FireClickEvent($e);
			if (this.options.fireInputTypeEvent) Element.FireInputTypeEvent($e);
			if (this.options.fireChangeEvent) Element.FireChangeEvent($e);
			if (this.options.fireKeyEnterEvent) Element.FireKeyEnterEvent($e);
		},
		GetAllMessages: function() {
			this.EnsureElements();
			var msgs = [];
			var messageFeeds = this.FetchMessageFeeds();
			for (let g = 0, gg = messageFeeds.length; g < gg; g++) {
				if (!messageFeeds[g]) continue;
				Array.prototype.push.apply(msgs, messageFeeds[g].querySelectorAll(this.MessageElement));
			}
			return msgs;
		},
		GetAllRecipientGuesses: function() {
			this.EnsureElements();
			var recipientHints = this.FetchRecipientHints();
			var hints = recipientHints || [];
			var guesses = [];

			for (var g = 0, gg = hints.length; g < gg; g++) {
				var hint = hints[g];
				if (!hint) continue;

				var guess = hint.value;

				if (!guess) {
					guess = hint.innerText;
				}

				if (guess)
					guesses.push(guess);
			}

			return guesses;
		},
		GetEncryptInputDatas: function() {
			this.EnsureElements();
			return this.Fetch(this.options.encryptInputDatas);
		},
		NewEncryptedMessages: function() {
			this.EnsureElements();
			// We check the message feed for new encrypted messages.
			// For this to work we need to remove the tokens for old failed messages - this is handled elsewhere.
			var messageFeeds = this.FetchMessageFeeds();
			for (let g = 0, gg = messageFeeds.length; g < gg; g++) {
				if (messageFeeds[g].innerText.indexOf(MORPHEUS_TOKEN_START) > -1
					|| messageFeeds[g].innerText.indexOf(PGP_TOKEN_START) > -1) {
					return true;
				}
			}
			return false;
		},
		SetCSS: function() {
			var css = this.CustomCSS;
			if (!css) return;

			var $style = document.querySelector('#morpheus-style');
			if (!$style) {
				$style = document.createElement("style");
				$style.type = "text/css";
				$style.id = "morpheus-style";
				document.head.appendChild($style);
			}

			$style.innerHTML = css;
		},
		SetModule: function(m) {
			// Guard to prevent multiple loadings of a custom module.
			// The message is sent multiple times as the script gets re-injected.

			this.CustomCSS = m.customCSS;
			this.SetCSS();

			if (this.ModuleLoaded) return false;
			this.ModuleLoaded = true;

			if (m.encryptInputDatas) {
				this.options.encryptInputDatas = m.encryptInputDatas;
			} else {
				// If we haven't specified any elements to put the encrypted data into,
				// we simply use the bind inputs.
				this.options.encryptInputDatas = m.bindInputs;
			}
			if (m.ensureElements) {
				this.options.ensureElements = m.ensureElements;
			}

			if (m.bindInputReplace != null) {
				this.BindInputReplace = m.bindInputReplace;
			}
			this.BindInputs = m.bindInputs;

			this.MessageFeeds = m.messageFeeds;

			this.MessageElement = m.messageElement;
			this.MessageText = m.messageText;

			this.RecipientHints = m.recipientHints;
			this.Resetters = m.resetters;

			return true;
		},
		SetResetters: function() {
			var $$resetters = this.Fetch(this.Resetters);

			for (var g = 0, gg = $$resetters.length; g < gg; g++) {
				if (Element.IsMorpheusModified($$resetters[g])) continue;
				Element.RegisterMorpheusEvent($$resetters[g], 'click', function(evt) {
					Morpheus.Reset();
				});
			}
		},
	};

	return ElementPicker;
})();

