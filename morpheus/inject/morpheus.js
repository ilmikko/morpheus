const Morpheus = (function() {
	return window.Morpheus = (function() {
		var Morpheus = {
			Automatic: false,
			BindEncryptInputs: function($$bindInput) {
				for (let g = 0, gg = $$bindInput.length; g < gg; g++) {
					let $sendInput = $$bindInput[g];
					if (!$sendInput || Element.IsMorpheusModified($sendInput)) continue;

					let $newSendInput = document.createElement('input');
					if (ElementPicker.BindInputReplace) {
						$newSendInput = Element.CloneNodeReplace($sendInput, $newSendInput);
					} else {
						$sendInput.parentNode.insertBefore($newSendInput, $sendInput);
						$sendInput.classList.add('morpheus-original-input');
						Element.CloneNodeRegister($newSendInput);

						// Trap focus in the old input so that the user never types directly into it.
						$sendInput.dataset.morpheusFocusBlock = true;
						Element.RegisterMorpheusEvent($sendInput, 'focus', function(e) {
							if (this.dataset.morpheusFocusBlock == true || this.dataset.morpheusFocusBlock == 'true') {
								console.log("Unencrypted input focus event blocked");
								e.stopPropagation();
								$newSendInput.focus();
							}
						});
					}

					$newSendInput.classList.add('morpheus-encrypted-input');

					let $lockIcon = Element.$SecurityIcon($newSendInput);
					$lockIcon.classList.add('morpheus-recipient-valid');

					Element.SecurityEnableEncryption($lockIcon);

					$lockIcon.addEventListener('click', function() {
						if ($lockIcon.classList.contains('secure')) {
							Element.MakeInsecure($newSendInput);
							Element.SecurityDisableEncryption($lockIcon);
						} else {
							Element.MakeSecure($newSendInput);
							Element.SecurityEnableEncryption($lockIcon);
						}
					});
					$newSendInput.parentNode.insertBefore($lockIcon, $newSendInput.nextSibling);

					// Stop any event leaking through the "secure input" input box.
					var stopEvents = ['click', 'paste', 'keypress', 'keydown', 'keyup', 'copy'];
					for (let g = 0, gg = stopEvents.length; g < gg; g++) {
						$newSendInput.addEventListener(stopEvents[g], function(e) {
							e.stopPropagation();
						});
					}

					function encryptAndSend(plaintext) {
						Send.Encrypt(plaintext)
							.then((cipher) => {
								sendWithoutEncrypt(cipher)
									.then(() => {
										if (Recipient.OneTimeSend()) {
											sendWithoutEncrypt(Recipient.NewRecipientMessage)
										}
									})
									.catch((err) => {
										alert(`(Error sending: ${err.message})`);
									});
							})
							.catch((err) => {
								alert(`(Error encrypting: ${err.message})`);
							});
					}

					function sendWithoutEncrypt(text) {
						return new Promise((accept, reject) => {
							try{
								$newSendInput.value = '';

								let $$encryptData = ElementPicker.GetEncryptInputDatas();
								for (let h = 0, hh = $$encryptData.length; h < hh; h++) {
									if (!$$encryptData[h]) { $$encryptData[h] = $sendInput }
									$$encryptData[h].textContent = $$encryptData[h].value = Convert.PGPToMor(text);
								}

								$sendInput.dataset.morpheusFocusBlock = false;

								// Send various change events to the real input
								ElementPicker.FireBindInputEvents($sendInput);

								$sendInput.dataset.morpheusFocusBlock = true;

								// Add some timeout before focusing again.
								setTimeout(() => {
									$newSendInput.focus();
									accept();
								}, 250);
							}
							catch(err) {
								reject(err);
							}
						});
					}

					$newSendInput.addEventListener('keydown', function(evt) {
						if (evt.keyCode == 13) { // Enter
							if (this.classList.contains('insecure')) {
								sendWithoutEncrypt(this.value)
									.catch((err) => {
										alert(`(Error sending: ${err.message})`);
									});
							} else {
								encryptAndSend(this.value);
							}
						}
					});

					setTimeout(function() {
						$newSendInput.focus();
					}, 1000);
				}
			},
			MarkEncryptedMessages: function($$messages) {
				while ($$messages.length > 0) {
					let $message = $$messages.shift();

					let $messageText = $message;
					if (ElementPicker.MessageText) {
						$messageText = $message.querySelector(ElementPicker.MessageText);
					}

					if (!$messageText) {
						continue;
					}

					if ($messageText.classList.contains('morpheus-message')) {
						// Already touched
						continue;
					}

					// Only mark for decrypt if message is actually encrypted.
					if (!Convert.IsCipher($messageText.innerText)) {
						// Remove any first time recipient messages.
						if (Recipient.IsRecipientMessage($messageText.innerText)) {
							$message.style.display = 'none';
						}
						continue;
					}

					let $newMessageText = Element.CloneNode($messageText);
					$newMessageText.innerText = $messageText.innerText;
					$newMessageText.classList.add('decryptable', 'morpheus-message');

					if ($message.parentNode == null) {
						$message = $newMessageText;
					}

					$newMessageText.addEventListener('dblclick', function() {
						if (Convert.IsMor(this.innerText)) {
							this.innerText = Convert.MorToPGP(this.innerText);
						}

						// Select entirety of ciphertext
						var range = document.createRange();
						range.selectNodeContents(this);

						var sel = window.getSelection();
						sel.removeAllRanges();
						sel.addRange(range);
					});

					let $lockIcon = Element.$LockIcon();
					$lockIcon.title = 'Press to decrypt';
					$lockIcon.addEventListener('click', function() {
						var self = this;
						Morpheus.DecryptMessage($newMessageText);
					});
					$message.parentNode.insertBefore($lockIcon, $message.nextSibling);

					$newMessageText.dataset.morpheusLockId = $lockIcon.dataset.morpheusId;
				}

				if (Morpheus.Automatic) {
					Morpheus.DecryptAllMessages();
				}
			},
			DecryptMessage: function($message) {
				let $lockIcon = ElementStore.Get($message.dataset.morpheusLockId);
				this.DecryptMessageText($message)
					.then((plain) => {
						$message.classList.add('morpheus-secure');
						$message.innerText = plain;
						$lockIcon.title = 'Secure PGP decrypted message';
						Element.MakeValid($lockIcon);
					})
					.catch((err) => {
						$message.classList.add('morpheus-warning');
						$message.innerText = "Decryption Error"; // TODO: How to retain the ciphertext without tripping up the new message detection?
						$lockIcon.title = `Error decrypting message: ${err.message}`;
						Element.MakeInvalid($lockIcon);
					});
			},
			DecryptMessageText: function($messageText) {
				if (!$messageText.classList.contains('decryptable')) {
					throw new Error('Tried to decrypt a non-decryptable message:', $messageText);
					return;
				}

				let ciphertext = $messageText.innerText.trim();
				$messageText.innerText = 'Decrypting...';
				$messageText.classList.remove('decryptable');

				let $$chunkMessages = [];
				let $potentialChunkMessage = $messageText.nextSibling;
				while (!Convert.IsCipher(ciphertext)) {
					// Chunked message.
					if (!$potentialChunkMessage) {
						console.warn('Message stream ended without ending a previous cipher!');
						console.warn(ciphertext);
						break;
					}

					let $potentialChunkMessageText = $potentialChunkMessage;
					if (ElementPicker.MessageText) {
						$potentialChunkMessageText = $potentialChunkMessage.querySelector(ElementPicker.MessageText);
					}

					if ($potentialChunkMessageText && $potentialChunkMessageText.innerText) {
						if (Convert.IsCipherStart($potentialChunkMessageText.innerText)) {
							console.warn('Discovered new starting token without ending a previous cipher!');
							console.warn(ciphertext);
							break;
						}

						ciphertext += $potentialChunkMessageText.innerText;

						if ($potentialChunkMessage.nodeType == 1) {
							// Hide any extra chunk message elements that contribute to the cipher.
							if (!$potentialChunkMessage.dataset.morpheusId) {
								$potentialChunkMessage = Element.CloneNode($potentialChunkMessage);
							}

							$potentialChunkMessageText.classList.remove('decryptable');
							$potentialChunkMessage.style.display = 'none';

							$$chunkMessages.push($potentialChunkMessage);
						}
					}

					if ($potentialChunkMessage.nextSibling) {
						$potentialChunkMessage = $potentialChunkMessage.nextSibling;
					} else {
						$potentialChunkMessage = $potentialChunkMessage.parentNode.nextSibling;
					}
				}

				// Convert Morpheus format back to PGP before decrypting.
				if (Convert.IsMor(ciphertext)) {
					ciphertext = Convert.MorToPGP(ciphertext);
				}

				return Send.Decrypt(ciphertext)
			},
			DecryptAllMessages: function() {
				// Decrypts all available encrypted messages.
				// If an error occurs, displays it in an icon next to the encrypted message.
				var $$messages = document.querySelectorAll('.morpheus-message.decryptable');

				for (var g = 0, gg = $$messages.length; g < gg; g++) {
					Morpheus.DecryptMessage($$messages[g]);
				}
			},
			ElementsGrab: function() {
				// TODO: allow for element-picking manually!
				Morpheus.VerifyRecipientText(ElementPicker.FetchRecipientHints());
				Morpheus.BindEncryptInputs(ElementPicker.FetchBindInputs());
				Morpheus.ObserveDecryptChanges(ElementPicker.FetchMessageFeeds());
				Morpheus.DecryptAllMessages()

				Send.Inject();
			},
			ElementsRelease: function() {
				Morpheus.ObserveDecryptChangesRevert();
				Element.RevertAll();
			},
			GuessSite: function() {
				var site = window.location.href;
				if (site) return site;
				return '';
			},
			GuessRecipients: function() {
				Recipient.Guesses = ElementPicker.GetAllRecipientGuesses();
				return Recipient.Guesses;
			},
			VerifyRecipientText: function($$recipientHints) {
				for (var g = 0, gg = $$recipientHints.length; g < gg; g++) {
					let $recipient = $$recipientHints[g];
					if (!$recipient) continue;

					let $newRecipient = Element.CloneNode($recipient);
					$newRecipient.classList.add('morpheus-recipient');
					$newRecipient.innerText = $recipient.innerText;

					let $validityIcon = Element.$ValidityIcon();
					$validityIcon.title = 'Recipient has not been verified';
					$newRecipient.appendChild($validityIcon);
				}
			},
			Message: function(m, s, r) {
				switch(m.command) {
					case 'automatic':
						this.SetAutomatic(m.automatic);
					case 'module':
						if (ElementPicker.SetModule(m)) {
							this.Reset();
						}
						break;
				}
			},
			ObserveDecryptChanges: function($$obsEl) {
				if (Morpheus.Observer) return;

				// Bind a listener to the DOM to decrypt any new messages.
				Morpheus.Observer = new MutationObserver(function(mutations) {
					for (let mutation of mutations) {
						if (!mutation.addedNodes) continue;
						if (ElementPicker.NewEncryptedMessages()) {
							console.log("Mutation observer found new messages to mark");
							Morpheus.MarkEncryptedMessages(ElementPicker.GetAllMessages());
							return;
						}
					}
				});

				for (let g = 0, gg = $$obsEl.length; g < gg; g++) {
					if (!$$obsEl[g]) continue;
					console.log("Observing changes on ", $$obsEl[g]);
					Morpheus.Observer.observe($$obsEl[g], {
						childList: true,
						subtree: true,
					});
				}
			},
			ObserveDecryptChangesRevert: function() {
				if (!Morpheus.Observer) return;
				console.log("Reverting observer");
				Morpheus.Observer.disconnect();
				delete Morpheus.Observer;
			},
			SetAutomatic: function(b) {
				if (b) {
					if (this.Automatic) return;
					this.Automatic = true;
					Morpheus.ElementsGrab()
				} else {
					if (!this.Automatic) return;
					this.Automatic = false;
					Morpheus.ElementsRelease()
				}
				Morpheus.MarkEncryptedMessages(ElementPicker.GetAllMessages());
			},
			SetValidity: function(validity) {
				var $$validityIcons = document.querySelectorAll('.morpheus-validity-icon');
				var $$recipients = document.querySelectorAll('.morpheus-recipient');

				for (var g = 0, gg = $$recipients.length; g < gg; g++) {
					if (validity[g] == true) {
						Element.MakeValid($$recipients[g]);
					} else {
						Element.MakeInvalid($$recipients[g]);
					}
				}
				for (var g = 0, gg = $$validityIcons.length; g < gg; g++) {
					if (validity[g] == true) {
						$$validityIcons[g].title = 'Recipient verified.';
						Element.MakeValid($$validityIcons[g]);
					} else {
						$$validityIcons[g].title = 'Recipient failed to verify.\nPlease press the Morpheus icon for more information.';
						Element.MakeInvalid($$validityIcons[g]);
					}
				}
			},
			SetSecurity: function(secure) {
				var $$recipientValid = document.querySelectorAll('.morpheus-recipient-valid');
				for (var g = 0, gg = $$recipientValid.length; g < gg; g++) {
					if (secure) {
						Element.SecurityDisableEncryption($$recipientValid[g]);
						Element.MakeValid($$recipientValid[g]);
						$$recipientValid[g].click();
					} else {
						$$recipientValid[g].title = 'There is a problem encrypting messages.\nPlease press the extension icon for more information.';
						Element.MakeInvalid($$recipientValid[g]);
					}
				}
			},
			Reset: function() {
				Morpheus.ObserveDecryptChangesRevert();

				ElementPicker.SetResetters();
				ElementPicker.SetCSS();

				setDebounce(function() {
					// Return state of injected Morpheus to master.
					Send.Inject()
						.then(state => {
							console.log(state);
							Recipient.Notified = !!state.notified;
							Recipient.SetRecipients(state.recipients);
							Morpheus.SetAutomatic(!!state.automatic);
							Morpheus.SetValidity(state.validity || []);
							Morpheus.SetSecurity(!!state.security);
						})
						.catch(err => {
							Morpheus.SetValidity([]);
							Morpheus.SetSecurity(false);
						});

					Morpheus.ObserveDecryptChanges(ElementPicker.FetchMessageFeeds());
					Morpheus.VerifyRecipientText(ElementPicker.FetchRecipientHints());
					Morpheus.BindEncryptInputs(ElementPicker.FetchBindInputs());

					Morpheus.MarkEncryptedMessages(ElementPicker.GetAllMessages());
				}, 100);
			},
			State: function() {
				console.log("Site guess:", this.GuessSite());
				console.log("Recipient guess:", this.GuessRecipients());
				return {
					site: this.GuessSite(),
					recipientGuesses: this.GuessRecipients(),
				};
			},
		};

		// Message listener from the background script
		browser.runtime.onMessage.addListener((m, s, r) => Morpheus.Message(m, s, r));

		return Morpheus;
	})();
})();
