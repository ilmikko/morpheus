if (window) {
	window.SiteDetector = SiteDetector;
}

// Fire one init event when body has loaded (for content scripts)
window.onload = function loadInit() {
	setTimeout(function() {
		try {
			Morpheus.Reset();
		}
		catch(err) {
			console.log("Injection failed; trying again...");
			loadInit();
		}
	}, 1000);
}

})();
Morpheus.Reset();
