/**
 * Recipient keeps track of notifying new recipients of Morpheus,
 * and removing any notifications for people who already use Morpheus.
 * */

const Recipient = (function() {
	const NEW_RECIPIENT_MESSAGE = `I have sent you an encrypted message using Morpheus. In order to decrypt, please follow: '${HELP_PAGE}#decrypt'.`;
	var Recipient = {
		NewRecipientMessage: NEW_RECIPIENT_MESSAGE,
		Notified: false,
		Guesses: [],
		Recipients: [],
		IsRecipientMessage: function(text) {
			return text.trim() == NEW_RECIPIENT_MESSAGE.trim();
		},
		OneTimeSend: function() {
			if (this.Notified) return false;
			this.Notified = true;
			Send.RecipientsNotified(this.Recipients);
			return true;
		},
		SetRecipients: function(s) {
			this.Recipients = s;
		},
	};
	return Recipient
})();
