/**
 * ElementStore keeps track of all elements that are modified by Morpheus.
 * */

const ElementStore = (function() {
	var nextId = (function() {
		var id=0;
		return function() {
			return id++;
		}
	})();

	var store = {};

	var ElementStore = {
		Get: function(id) {
			return store[id];
		},
		Put: function(el) {
			if (el.dataset.morpheusId) return el.dataset.morpheusId;

			var id = nextId();
			store[id] = el;
			el.dataset.morpheusId = id;

			return id;
		},
		Remove: function(id) {
			var el = store[id];
			if (!el) return null;

			store[id] = null;
			el.removeAttribute('data-morpheus-id');

			return el;
		},
	};
	return ElementStore;
})();

