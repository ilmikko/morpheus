/**
 * Element is in charge of all more complex DOM element manipulations.
 * */

const Element = (function() {
	var ClonedNodes = {};
	var ReplacedNodes = {};
	var NodeEvents = {};

	var Element = {
		$LockIcon: function() {
			let $lock = document.createElement('span');
			$lock.className = 'morpheus-lock-icon';
			this.CloneNodeRegister($lock);
			return $lock;
		},
		$SecurityIcon: function() {
			let $lock = document.createElement('span');
			$lock.className = 'morpheus-security-icon';
			this.CloneNodeRegister($lock);
			return $lock;
		},
		$ValidityIcon: function() {
			let $validityIcon = document.createElement('span');
			$validityIcon.className = 'morpheus-validity-icon';
			this.CloneNodeRegister($validityIcon);
			return $validityIcon;
		},
		CloneNode: function($n) {
			return this.CloneNodeReplace($n, $n.cloneNode());
		},
		CloneNodeRegister: function($n) {
			var id = ElementStore.Put($n);
			ClonedNodes[id] = $n;
			return id;
		},
		CloneNodeDeregister: function(id) {
			delete ClonedNodes[id];
			return ElementStore.Remove(id);
		},
		CloneNodeReplace: function($o, $n) {
			$o.parentNode.replaceChild($n, $o);
			var id = this.CloneNodeRegister($n);
			ReplacedNodes[id] = $o;
			return $n;
		},
		CloneNodeRevert: function(id) {
			var $n = this.CloneNodeDeregister(id);

			// Replace with old node if possible.
			var $oldNode = ReplacedNodes[id];
			if ($oldNode) {
				$n.parentNode.replaceChild($oldNode, $n);
			}

			$n.remove();
			return $n;
		},
		EnsureExistence: function(query) {
			// EnsureExistence only works with direct children, e.g. p > span.
			// However, the root part of the query can be of arbitrary complexity, as long as the root element is found.
			let parts = query.split('>');
			let root = parts.shift();
			let el = document.querySelector(root.trim());
			if (!el) throw "Root node not found for ensure element query " + query;
			while(parts.length > 0) {
				let next = parts.shift()
				root += next;
				let ch = el.querySelector(root.trim());
				if (!ch) {
					console.log("Ensuring " + next.trim());
					ch = document.createElement(next.trim());
					el.appendChild(ch);
				}
				el = ch
			}
		},
		FireEvent: function($e, handler, o) {
			o = o || {};

			if (o.bubbles == null) o.bubbles = true;
			if (o.cancelable == null) o.cancelable = true;

			if ('createEvent' in document) {
				var eventType = {
					'change': Event,
					'click': MouseEvent,
					'focus': FocusEvent,
					'mouseup': MouseEvent,
					'keydown': KeyboardEvent,
					'keyup': KeyboardEvent,
					'keypress': KeyboardEvent,
					'input': InputEvent,
				}
				$e.dispatchEvent(new eventType[handler](handler, o));
			} else {
				$e.fireEvent('on' + handler, o);
			}
		},
		FireChangeEvent: function($e) {
			this.FireEvent($e, 'change')
		},
		FireClickEvent: function($e) {
			this.FireEvent($e, 'click', {
				target: $e,
			});
		},
		FireFocusEvent: function($e) {
			this.FireEvent($e, 'focus', {
				target: $e,
			});
		},
		FireInputTypeEvent: function($e) {
			this.FireEvent($e, 'input', {
				target: $e,
			});
		},
		FireKeyEnterEvent: function($e) {
			this.FireEvent($e, 'keydown', {
				target: $e,
				keyCode: 13,
			});
		},
		Get: function(id) {
			// Alias to minimize coupling.
			return ElementStore.Get(id);
		},
		IsMorpheusModified: function($e) {
			return !!$e.dataset.morpheusId;
		},
		MakeValid: function($n) {
			$n.classList.remove('invalid');
			$n.classList.add('valid');
		},
		MakeInvalid: function($n) {
			$n.classList.remove('valid');
			$n.classList.add('invalid');
		},
		MakeSecure: function($n) {
			$n.classList.remove('insecure');
			$n.classList.add('secure');
		},
		MakeInsecure: function($n) {
			$n.classList.remove('secure');
			$n.classList.add('insecure');
		},
		// Used to keep track of event listeners we add, so we can revert them later.
		RegisterMorpheusEvent: function($e, name, cbk) {
			var id = ElementStore.Put($e);

			if (!NodeEvents[id]) NodeEvents[id] = [];

			NodeEvents[id].push({
				name: name,
				callback: cbk,
			});

			$e.addEventListener(name, cbk);
		},
		RevertAll: function() {
			for (var id in ClonedNodes) {
				this.CloneNodeRevert(id);
			}
			for (var id in NodeEvents) {
				var $el = ElementStore.Remove(id);
				if (!$el) continue;

				var events = NodeEvents[id];
				for (var g = 0, gg = events.length; g < gg; g++) {
					var name = events[g].name;
					var cbk = events[g].callback;
					$el.removeEventListener(name, cbk);
				}
			}
		},
		SecurityEnableEncryption: function($l) {
			Element.MakeSecure($l);
			$l.title = 'Messages will be encrypted and secure.\nPress to disable encryption.';
		},
		SecurityDisableEncryption: function($l) {
			Element.MakeInsecure($l);
			$l.title = 'Messages will not be encrypted!\nPress to enable encryption.';
		},
	};

	return Element;
})();

