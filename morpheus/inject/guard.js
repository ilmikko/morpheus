/**
 * Make sure all constants are defined only once per inject.
 * */

(function Guard(){
	// Only run once per inject.
	if (window.Morpheus) return;
