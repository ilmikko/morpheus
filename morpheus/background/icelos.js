const Icelos = (function() {
	var Icelos = {
		Version: "",
		Decrypt: function(c) {
			return HTTP.Post("/decrypt", {
				cipher: c,
			});
		},
		Encrypt: function(msg, keys) {
			return HTTP.Post("/encrypt", {
				message: msg,
				keys: keys,
			});
		},
		GetState: function() {
			return new Promise((accept, reject) => {
				HTTP.Get("/version")
					.then((res) => {
						Icelos.Version = res;
						accept({
							connected: true,
						})
					})
					.catch(reject)
			})
		},
		Init: function() {
			Icelos.GetState()
				.catch(Send.Error);
		},
		Keys: function(rs) {
			return new Promise((accept, reject) => {
				HTTP.Post("/keys", {
					recipients: rs,
				})
					.then(res => accept(JSON.parse(res)))
					.catch(err => reject(err));
			});
		},
	};

	return Icelos;
})();
