const Morpheus = (function() {
	var Morpheus = {
		Init: function() {
			Icelos.Init();
			browser.runtime.onMessage.addListener((m, s, r) => r(Morpheus.Message(m, s) || new Promise()));

			// Set default settings.
			if (!localStorage.getItem(ADVANCED_MODE)) {
				localStorage.setItem(ADVANCED_MODE, ADVANCED_MODE_DEFAULT);
			}
			if (!localStorage.getItem(AUTO_DECRYPT)) {
				localStorage.setItem(AUTO_DECRYPT, AUTO_DECRYPT_DEFAULT);
			}
			if (!localStorage.getItem(HELP_MESSAGES)) {
				localStorage.setItem(HELP_MESSAGES, HELP_MESSAGES_DEFAULT);
			}
		},
		GetState: function(full) {
			return new Promise((accept, reject) => {
				TabData.Get()
					.then((data) => {
						Icelos.GetState()
							.then((istate) => {
								// If we found Icelos then we're active.
								data.active = istate.connected;

								var errors = data.errors || [];

								// TODO: Replace with getstate and send state
								// All recipients that have an empty error message are valid
								data.validity = errors.map(e => e == '');
								// If there is a single error, our security status is false, but the user
								// can still send messages to a limited set of people in the group.
								data.security = errors.reduce((k, p) => k + p, '') == '';

								data.errors = errors;

								// Check against our storage whether all of the recipients have been notified.
								if (localStorage.getItem(HELP_MESSAGES) == 'true') {
									data.notified = data.recipients.map(k => this.IsNotified(k)).reduce((k, p) => k && p, true);
								} else {
									data.notified = true;
								}

								// Popup can see the key IDs, but we won't share that information with the
								// background script for security reasons. (we don't want the page to know
								// information about our keyring, only whether it's correct or not.)
								if (!full) {
									delete data.errors;
									delete data.recipients;
									delete data.keys;
								}

								data.automatic = localStorage.getItem(AUTO_DECRYPT) == 'true';

								accept(data);
							})
							.catch((err) => reject(err));
					})
					.catch((err) => reject(err));
			});
		},
		IsNotified: function(rcpt) {
			return !!localStorage.getItem(`notified-${rcpt}`);
		},
		Message: function(m, s) {
			// TODO: use sender (s) to better determine some methods and prevent infinite loops
			switch(m.command) {
				case 'automatic':
				case 'module':
					return Send.Tab(m);
				case 'decrypt':
					return Icelos.Decrypt(m.cipher);
				case 'encrypt':
					return new Promise((accept, reject) => {
						TabData.Get()
							.then((data) => {
								if (!m.keys) m.keys = data.keys;

								Icelos.Encrypt(m.message, m.keys)
									.then(accept)
									.catch(reject);
							});
					});
				case 'inject':
					return new Promise((accept, reject) => {
						TabData.Get()
							.then((data) => {
								var ownKey = localStorage.getItem(OWN_KEY) || '';

								if (!data.recipients || data.recipients.length == 0) {
									data.recipients = [ownKey];
								}

								ownKey = data.recipients[data.recipients.length - 1];

								if (m.state.recipientGuesses) {
									data.recipients = m.state.recipientGuesses.map((guess) => {
										var rcpt = localStorage.getItem(RECIPIENT_MAP + guess);
										if (!rcpt) {
											rcpt = guess;
										}
										return rcpt;
									});
									data.recipients.push(ownKey);
								}

								Morpheus.UpdateEncryptionKeys(data)
									.then(() => {
										Morpheus.GetState(true)
											.then((res) => accept(res))
											.catch((err) => reject(err));
									})
									.catch((err) => reject(err));
							})
							.catch((err) => reject(err));
					});
				case 'keys':
					return new Promise((accept, reject) => {
						TabData.Set({
							recipients: m.recipients,
						})
							.then((data) => {
								Morpheus.UpdateEncryptionKeys(data)
									.then(accept)
									.catch(reject);
							})
							.catch((err) => {
								reject(err);
							});
					});
				case 'recipients-notified':
					return new Promise((accept, reject) => {
						for (let g = 0, gg = m.recipients.length; g < gg; g++) {
							this.SetNotified(m.recipients[g]);
						}
					});
				case 'state':
					return Morpheus.GetState(true);
				case 'validate':
					return new Promise((accept, reject) => {
						Icelos.Keys(m.recipients)
							.then(res => {
								accept(res);
							})
							.catch(err => {
								reject(err);
							})
					});
			}
		},
		SetNotified: function(rcpt) {
			localStorage.setItem(`notified-${rcpt}`, true);
		},
		UpdateEncryptionKeys: function(data) {
			return new Promise((accept, reject) => {
				Icelos.Keys(data.recipients)
					.then(res => {
						// We never send the actual public keys to the tab for security reasons.
						// The tab will prompt the user to check the Morpheus popup for
						// more information if there are errors.

						// Own key is appended to the end, remove that
						res.Errors.pop();

						TabData.Set({
							errors: res.Errors || [],
							keys: res.Keys.filter(k => k),
							recipients: data.recipients,
						})
							.finally(() => accept(res));
					})
					.catch(res => {
						TabData.Set({
							errors: res.Errors || [],
							recipients: data.recipients,
						})
							.finally(() => reject(res));
					});
			})
		},
	};

	return Morpheus;
})();

Morpheus.Init();
