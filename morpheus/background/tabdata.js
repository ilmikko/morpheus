// Helper to handle communication between multiple tabs.
const TabData = (function() {
	var DATA = {};
	function extend(a, b) {
		for (let g in b) {
			a[g] = b[g];
		}
		return a;
	}
	var TabData = {
		Get: function() {
			return new Promise((accept, reject) => {
				browser.tabs.query({
					active: true,
					currentWindow: true,
				})
					.then((tabs) => {
						accept(DATA[tabs[0].id] || {});
					})
					.catch(reject);
			});
		},
		Set: function(o) {
			return new Promise((accept, reject) => {
				browser.tabs.query({
					active: true,
					currentWindow: true,
				})
					.then((tabs) => {
						let id = tabs[0].id;

						DATA[id] = DATA[id] || {};

						extend(DATA[id], o);
						accept(DATA[id]);
					})
					.catch(reject);
			});
		},
	};
	return TabData;
})();
