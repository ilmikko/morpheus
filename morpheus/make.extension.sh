#!/usr/bin/env bash
cd "$(dirname $0)";
EXT="extension";
EXTSRC="$EXT/src";
EXTIMG="$EXT/img";
EXTMOD="$EXT/mod";

echo "Building Extension...";
rm -rf "./$EXT";
mkdir -p "$EXT" \
	"$EXTIMG" \
	"$EXTSRC" \
	"$EXTSRC/default" \
	"$EXTSRC/settings" \
	"$EXTSRC/status" \
	"$EXTSRC/welcome" \
	"$EXTMOD" \

# Manifest.
cat "common/manifest.template.json" > "$EXT/manifest.json";

# Image assets.
cp -rv "img/." "$EXTIMG";

# Morpheus background script.
cat "common/constants.js" \
	"common/polyfill.js" \
	"common/http.js" \
	"common/send.js" \
	"common/site-detector.js" \
	"background/icelos.js" \
	"background/tabdata.js" \
	"background/morpheus.js" > "$EXTSRC/background.js";

# Morpheus popup script.
cat "common/constants.js" \
	"popup/default/popup.js" > "$EXTSRC/default/popup.js";

cat "popup/default/popup.css" > "$EXTSRC/default/popup.css";
cat "popup/default/popup.html" > "$EXTSRC/default/popup.html";

# Morpheus status script.
cat "common/constants.js" \
	"common/polyfill.js" \
	"common/send.js" \
	"common/debounce.js" \
	"inject/element.js" \
	"popup/status/popup.js" > "$EXTSRC/status/popup.js";

cat "common/popup.css" \
	"popup/status/popup.css" \
	"inject/inject.css" > "$EXTSRC/status/popup.css";

cat "popup/status/popup.html" > "$EXTSRC/status/popup.html";

# Morpheus settings script.
cat "common/constants.js" \
	"common/polyfill.js" \
	"common/http.js" \
	"common/send.js" \
	"popup/settings/popup.js" > "$EXTSRC/settings/popup.js";

cat "common/popup.css" \
	"popup/settings/popup.css" \
	"inject/inject.css" > "$EXTSRC/settings/popup.css";

cat "popup/settings/popup.html" > "$EXTSRC/settings/popup.html";

# Morpheus welcome script.
cat "common/constants.js" \
	"common/polyfill.js" \
	"common/debounce.js" \
	"common/http.js" \
	"common/send.js" \
	"popup/welcome/popup.js" > "$EXTSRC/welcome/popup.js";

cat "common/popup.css" \
	"popup/welcome/popup.css" \
	"inject/inject.css" > "$EXTSRC/welcome/popup.css";

cat "popup/welcome/popup.html" > "$EXTSRC/welcome/popup.html";

# Morpheus inject script.
cat "inject/guard.js" \
	"common/constants.js" \
	"common/polyfill.js" \
	"common/convert.js" \
	"common/debounce.js" \
	"common/send.js" \
	"common/site-detector.js" \
	"inject/element_store.js" \
	"inject/element_picker.js" \
	"inject/element.js" \
	"inject/recipient.js" \
	"inject/morpheus.js" \
	"inject/guard_end.js" > "$EXTSRC/inject.js";

# Morpheus content script.
# The content script is just an inject script with all the potential modules attached.
# This is because a background script does not have the permissions to execute
# the module scripts after the content script has been injected; instead we will just
# execute the module directly after the inject script.
cat "inject/guard.js" \
	"common/constants.js" \
	"common/polyfill.js" \
	"common/convert.js" \
	"common/debounce.js" \
	"common/send.js" \
	"common/site-detector.js" \
	"inject/element_store.js" \
	"inject/element_picker.js" \
	"inject/element.js" \
	"inject/recipient.js" \
	"inject/morpheus.js" \
	"module/"*".js" \
	"inject/guard_end.js" > "$EXTSRC/content.js";

cat "inject/inject.css" > "$EXTSRC/inject.css";

# Morpheus modules.
cp -rv "module/." "$EXTMOD";

# Package the module.
[ -f "$EXT.zip" ] && rm "$EXT.zip";
(
cd "$EXT";
zip -0 -r "../$EXT.zip" *
)
