# Morpheus

Interfaces directly with the messaging platform.
It loads different "modules" based on which platform it detects.
By default open for all URLs (essentially available for all IM by default)

Communicates with the backend via HTTP requests, mainly to talk with GPG on the machine.
Why HTTP? Well, this seems like the best cross-platform solution.
Morpheus only needs a port to talk to the machine, and this can be running in a Docker container on Windows or natively on UNIX etc., and still works.
