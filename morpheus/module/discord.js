if (SiteDetector.Current() == 'discord')
	browser.runtime.sendMessage({
		command: 'module',
		// Make the inputs look closer to Discord's style.
		customCSS: `
		.placeholder-37qJjk {
			pointer-events: none;
		}
		.morpheus-encrypted-input {
			height: 100%;
			width: calc(100% - 40px);
		}
		`,
		bindInputReplace: false,
		bindInputs: [
			'div[contenteditable=true][aria-multiline]',
		],
		encryptInputDatas: [
			'div[contenteditable=true][aria-multiline] > div > span > span > span',
		],
		messageFeeds: [
			'.chatContent-a9vAAp',
		],
		messageElement: '.messageContent-2qWWxC',
		messageText: '.messageContent-2qWWxC',
		recipientHints: [
			'h3[role=button]',
		],
		resetters: '.privateChannels-1nO12o',
	});
