if (SiteDetector.Current() == 'facebook-messenger')
	browser.runtime.sendMessage({
		command: 'module',
		// Make the inputs look closer to Messenger's style.
		customCSS: `
		._1p1t {
			opacity: 0;
		}
		.morpheus-encrypted-input {
			font-size: 14px;
			padding: 8px 9px;
			border-radius: 18px;
			border: 0;
			background: rgba(11, 234, 5, 0.2);
			width: calc(100% - 77px);
		}
		.clearfix._o46._3i_m > .morpheus-lock-icon {
			float: right;
		}
		[aria-label="Type a message..."] {
			height: 2px;
			overflow: hidden;
		}
		`,
		bindInputReplace: false,
		bindInputs: [
			'[aria-label="Type a message..."]',
		],
		encryptInputDatas: [
			'div[data-contents="true"] > div > div > span > span',
		],
		ensureElements: [
			// Use a custom paragraph to interact with the Messenger contenteditable div.
			// React crashes if we try to write directly into the span.
			'div[data-contents="true"] > div > div > span > span',
		],
		messageFeeds: [
			'[role="presentation"]',
		],
		messageElement: '.clearfix._o46._3erg > div',
		recipientHints: [
			'._3eur._6ybk',
		],
		resetters: '[aria-label="Conversation list"] > li',
	});
