if (SiteDetector.Current() == 'telegram-web')
	browser.runtime.sendMessage({
		command: 'module',
		// Make the inputs look closer to Telegram Web's style.
		customCSS: `
		.morpheus-lock-icon {
			float: right;
		}
		.morpheus-secure.im_message_text {
			filter: none;
			background: #cfc;
			padding: 3px;
			border-radius: 5px;
		}
		.morpheus-encrypted-input {
			width: calc(100% - 60px);
			background: #cfc;
			border-radius: 5px;
			border: 0;
			padding: 5px;
		}
		.morpheus-recipient.invalid {
			color: #fab;
		}
		.morpheus-recipient.valid {
			color: #cfc;
		}
		.composer_rich_textarea.morpheus-original-input {
			opacity: 0;
		}
		`,
		bindInputReplace: false,
		bindInputs: [
			'.composer_rich_textarea',
		],
		messageFeeds: [
			'.im_history_selected_wrap',
		],
		messageElement: '.im_history_message_wrap .im_message_body',
		messageText: '.im_message_text',
		recipientHints: [
			'.tg_head_peer_title',
		],
		resetters: 'ul.nav > li.im_dialog_wrap',
	});
