if (SiteDetector.Current() == 'morpheus-test')
	browser.runtime.sendMessage({
		command: 'module',
		customCSS: `
		body {
			border: 2px solid #0f02;
		}
		.morpheus-lock-icon {
			color: yellow;
		}
		`,
		bindInputReplace: false,
		bindInputs: [
			'#sendmsg',
		],
		messageFeeds: [
			'body',
		],
		messageElement: '.message',
		recipientHints: '#members > p',
		resetters: '#selector input',
	});
