if (SiteDetector.Current() == 'whatsapp-web')
	browser.runtime.sendMessage({
		command: 'module',
		// Make the inputs look closer to Telegram Web's style.
		customCSS: `
		._39LWd {
			opacity: 0;
		}
		#main div[contenteditable] {
			width: 0;
		}
		.morpheus-encrypted-input {
			width: 100%;
		}
		.morpheus-lock-icon {
			text-shadow: 0.8px 0.7px 0px #0008;
			margin-top: -30px;
		}
		.message-in + .morpheus-lock-icon {
			float: left;
		}
		.message-out + .morpheus-lock-icon {
			float: right;
		}
		.morpheus-security-icon {
			padding-top: 6px;
		}
		`,
		bindInputReplace: false,
		bindInputs: [
			'#main div[contenteditable]',
		],
		messageFeeds: [
			'#main',
		],
		messageElement: '.message-in,.message-out',
		messageText: '.message-in span.selectable-text,.message-out span.selectable-text',
		recipientHints: [
			'#main > header span[dir="auto"]',
		],
		resetters: '#pane-side',
	});
