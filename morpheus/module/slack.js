if (SiteDetector.Current() == 'slack')
	browser.runtime.sendMessage({
		command: 'module',
		// Make the inputs look closer to Telegram Web's style.
		customCSS: `
		div[role="main"] .ql-placeholder {
			opacity: 0;
		}
		.morpheus-encrypted-input {
			width: calc(100% - 50px);
			border: 0;
		}
		.morpheus-secure {
			background-color: #f9ffd366;
		}
		`,
		bindInputReplace: false,
		bindInputs: [
			'div[role="main"] div[contenteditable=true]',
		],
		messageFeeds: [
			'div[data-qa="message_pane"]',
		],
		messageElement: 'div[data-qa="message_pane"] div[data-qa="message_content"]',
		messageText: 'div[data-qa="message_content"] > div',
		recipientHints: [
			'div[data-qa="channel_name"] span',
		],
		resetters: 'div[data-qa="slack_kit_list"]',
	});
