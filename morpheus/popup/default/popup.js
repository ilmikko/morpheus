(function MaybeShowWelcome() {
	var welcomeShown = localStorage.getItem(WELCOME_SHOWN) == 'true';
	if (!welcomeShown) {
		window.location = '../welcome/popup.html';
	} else {
		window.location = '../status/popup.html';
	}
})()
