const $ownKey = document.querySelector('#own-key');
const $ownKeyValidity = document.querySelector('#own-key-validity');

const $autoDecrypt = document.querySelector('#autodecrypt');
const $advanced = document.querySelector('#advanced');
const $helpMessages = document.querySelector('#help-messages');

var Settings = {
	Init: function() {
		var self = this;

		browser.runtime.onMessage.addListener((message) => {
			Settings.Message(message);
		});

		$ownKey.addEventListener('input', function() {
			localStorage.setItem('own-key', $ownKey.value);
		});
		$ownKey.value = localStorage.getItem('own-key');

		$advanced.addEventListener('change', function() {
			self.Advanced(this.checked);
		})
		this.Advanced(localStorage.getItem(ADVANCED_MODE) == 'true');

		$autoDecrypt.addEventListener('change', function() {
			self.AutoDecrypt(this.checked);
		})
		this.AutoDecrypt(localStorage.getItem(AUTO_DECRYPT) == 'true');

		$helpMessages.addEventListener('change', function() {
			self.HelpMessages(this.checked);
		})
		this.HelpMessages(localStorage.getItem(HELP_MESSAGES) == 'true');
	},
	Advanced: function(b) {
		localStorage.setItem(ADVANCED_MODE, b);
		$advanced.checked = b;
	},
	AutoDecrypt: function(b) {
		localStorage.setItem(AUTO_DECRYPT, b);
		$autoDecrypt.checked = b;
		Send.Automatic(b);
	},
	HelpMessages: function(b) {
		localStorage.setItem(HELP_MESSAGES, b);
		$helpMessages.checked = b;
	},
	Message: function(m) {
		switch(m.command) {}
	},
}

Settings.Init()
