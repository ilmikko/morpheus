const $ownKey = document.querySelector('#own-key');
const $ownKeyValidity = document.querySelector('#own-key-validity');

const $recipients = document.querySelector('#recipients');
const $nextRecipient = document.querySelector('#next-recipient');

const $message = document.querySelector('#message');
const $cipher = document.querySelector('#cipher');

const $advanced = document.querySelector('#advanced');

var Morpheus = {
	initControls: function() {
		var $encrypt = document.querySelector('#encrypt');
		var $decrypt = document.querySelector('#decrypt');

		$encrypt.addEventListener('click', function() {
			if ($message.value == "") return;
			Morpheus.SendEncrypt($message.value, recipients);
		});

		$decrypt.addEventListener('click', function() {
			if ($cipher.value == "") return;
			Morpheus.SendDecrypt($cipher.value);
		});

		$ownKey.addEventListener('input', function() {
			setDebounce(() => {
				Morpheus.ValidateRecipients()
					.then(() => {
						localStorage.setItem(OWN_KEY, $ownKey.value);
					});
			}, 500);
		});
		$ownKey.value = localStorage.getItem(OWN_KEY);

		this.AdvancedControls(localStorage.getItem('advanced-mode') == "true");
	},
	AdvancedControls: function(b) {
		if (b) {
			$advanced.style.display = 'initial';
		} else {
			$advanced.style.display = 'none';
		}
	},
	ClearRecipients: function() {
		this.ShowRecipients([localStorage.getItem(OWN_KEY)]);
		this.SendKeys([], []);
		this.ValidateRecipients();
	},
	ShowRecipients: function(recipients) {
		while ($recipients.firstChild) $recipients.removeChild($recipients.firstChild);

		// The last recipient is our own key.
		var ownKey = recipients.pop();
		if (ownKey) {
			$ownKey.value = ownKey;
			localStorage.setItem(OWN_KEY, ownKey);
		}

		for (var g = 0, gg = recipients.length; g < gg; g++) {
			var $div = document.createElement('div');
			$div.className = 'row';

			var $rcpt = document.createElement('input');
			$rcpt.id = "recipient-" + g;
			$rcpt.className = 'morpheus-recipient';
			$rcpt.value = recipients[g];
			let old = recipients[g];
			$rcpt.addEventListener('input', function() {
				setDebounce(() => {
					localStorage.setItem(RECIPIENT_MAP + old, $rcpt.value);
					Morpheus.ValidateRecipients();
				}, 500);
			})

			var $validity = document.createElement('span');
			$validity.className = 'morpheus-validity-icon';

			$div.appendChild($rcpt);
			$div.appendChild($validity);

			$recipients.appendChild($div);
		}
	},
	ValidateRecipients: function() {
		var $$recipient = document.querySelectorAll('.morpheus-recipient');
		var rcpts = [];

		for (var g = 0, gg = $$recipient.length; g < gg; g++) {
			rcpts.push($$recipient[g]);
		}

		return new Promise((accept, reject) => {
			Morpheus.ValidateInputs(rcpts)
				.then((names, keys) => {
					Morpheus.SendKeys(names, keys);
				})
				.catch(err => reject(err));
		});
	},
	Init: function() {
		browser.runtime.onMessage.addListener((message) => {
			Morpheus.Message(message);
		});

		/**
		 * Inject the content script into the active tab.
		 * If we couldn't inject the script, handle (display) the error.
		 */
		browser.tabs.executeScript({
			file: '/src/inject.js'
		}).catch(Morpheus.RecvError);

		Send.GetState()
			.then((res) => Morpheus.RecvState(res))
			.catch((err) => Morpheus.RecvError(err));

		this.initControls();
	},
	Message: function(m) {
		switch(m.command) {
			case 'error':
				Morpheus.RecvError(m.error);
				break;
		}
	},
	SendAutomatic: function(b) {
		browser.runtime.sendMessage({
			command: 'automatic',
			automatic: b,
		});
	},
	SendDecrypt: function(c) {
		browser.runtime.sendMessage({
			command: 'decrypt',
			cipher: c,
		})
			.then(Morpheus.RecvPlain)
			.catch(Morpheus.RecvError);
	},
	SendEncrypt: function(msg, rcpt) {
		browser.runtime.sendMessage({
			command: 'encrypt',
			message: msg,
		})
			.then(Morpheus.RecvCipher)
			.catch(Morpheus.RecvError);
	},
	SendKeys: function(rs, k) {
		return browser.runtime.sendMessage({
			command: 'keys',
			recipients: rs,
			keys: k,
		});
	},
	SendValidateRecipients: function(rs) {
		return browser.runtime.sendMessage({
			command: 'validate',
			recipients: rs,
		});
	},
	RecvActive: function(b) {
		var $status = document.querySelector('#status');
		var $lockDisabled = document.querySelector('#lock-disabled');
		var $lockEnabled = document.querySelector('#lock-enabled');

		if (b) {
			$lockDisabled.style.display = 'none';
			$lockEnabled.style.display = 'initial';
			$status.innerText = 'Enabled';
			$status.className = 'enabled';
		} else {
			$lockDisabled.style.display = 'initial';
			$lockEnabled.style.display = 'none';
			$status.innerText = 'Disabled';
			$status.className = 'disabled';
		}
	},
	RecvAutomatic: function(b) {
		// Clear all recipients when not automatic.
		if (!b) Morpheus.ClearRecipients();
	},
	RecvCipher: function(c) {
		$message.value = '';
		$cipher.value = c;
	},
	RecvError: function(error) {
		document.querySelector('#controls').classList.add('hidden');

		var $error = document.querySelector('#error');

		$error.classList.remove('hidden');
		if (error.helpfulMessage) {
			$error.innerText = error.helpfulMessage;
		} else {
			$error.innerText = `Morpheus encountered an unexpected error.\n${error.message}\nPlease see ${HELP_PAGE} for more information.`;
		}

		console.error(`Morpheus error: ${error.message}`);
	},
	RecvPlain: function(p) {
		$message.value = p;
		$cipher.value = '';
	},
	RecvState: function(s) {
		this.RecvActive(!!s.active);
		this.RecvAutomatic(!!s.automatic);

		if (s.recipients) {
			this.ShowRecipients(s.recipients);
		}

		var $site = document.querySelector('#site');

		if (s.site) {
			$site.innerText = `Site detected: ${s.site}`;
		} else {
			$site.innerText = '';
		}

		this.ValidateRecipients();
	},
	ValidateInputs: function($$input) {
		let values = [];

		for (let g = 0, gg = $$input.length; g < gg; g++) {
			let $input = $$input[g];
			let $validity = document.querySelector(`#${$input.id} + .morpheus-validity-icon`)

			$input.classList.remove('valid', 'invalid');
			$validity.classList.remove('valid', 'invalid');

			values.push($input.value);
		}

		return new Promise((accept, reject) => {
			Morpheus.SendValidateRecipients(values)
				.then(res => {
					for (let g = 0, gg = $$input.length; g < gg; g++) {
						let $input = $$input[g];
						let $validity = document.querySelector(`#${$input.id} + .morpheus-validity-icon`)

						if (res.Errors[g] == "") {
							$input.classList.add('valid');
							$validity.classList.add('valid');
							$validity.title = `${res.Names[g]} (${res.Keys[g]})`;
						} else {
							$input.classList.add('invalid');
							$validity.classList.add('invalid');
							$validity.title = "Error: " + res.Errors[g].replace(/\:/g, ':\n');
						}
					}

					accept(res.Recipients, res.Keys);
				})
				.catch(err => {
					for (let g = 0, gg = $$input.length; g < gg; g++) {
						let $input = $$input[g];
						let $validity = document.querySelector(`#${$input.id} + .morpheus-validity-icon`)
						$input.classList.add('invalid');
						$validity.classList.add('invalid');
						$validity.title = "Error: " + err.message;
					}
					reject(err);
				});
		});
	},
}

Morpheus.Init()
