const Welcome = (function() {
	let Welcome = {
		Init: function() {
			this.SetupIcelos().then(() => {
				this.SetupKeys().then(() => {
					this.SetupDone().then(() => {
						window.location = '../status/popup.html';
					});
				});
			});
		},
		SetupDone: function() {
			localStorage.setItem(WELCOME_SHOWN, 'true');
			return new Promise((accept, reject) => {
				let $lockDisabled = document.querySelector('#lock-disabled');
				let $lockEnabled = document.querySelector('#lock-enabled');
				let $lockSetup = document.querySelector('#lock-setup');
				let $status = document.querySelector('#status');

				$status.innerText = "You're all set!";
				$lockEnabled.style.animation = 'lock 500ms ease-in forwards';
				$lockSetup.classList.add('hidden');

				setTimeout(() => {
					accept();
				}, 2000);
			});
		},
		SetupIcelos: function() {
			return new Promise((accept, reject) => {
				let $icelosSection = document.querySelector('#icelos-section');
				let $icelosStatus = document.querySelector('#icelos-status');
				let $icelosError = document.querySelector('#icelos-error');

				$icelosSection.classList.remove('hidden');
				$icelosStatus.innerText = 'Connecting...';
				Send.GetState()
					.then((res) => {
						$icelosStatus.innerText = `Icelos Setup OK.`;
						accept();
					})
					.catch((err) => {
						$icelosStatus.innerText = `Error: ${err.message}`;
						$icelosError.innerHTML = `Please follow the steps in <a href="${HELP_PAGE}">${HELP_PAGE}</a> to set up Icelos.`;
					});
			});
		},
		SendKeys: function() {
			return browser.runtime.sendMessage({
				command: 'keys',
				recipients: rs,
				keys: k,
			});
		},
		SendValidate: function(rs) {
			return browser.runtime.sendMessage({
				command: 'validate',
				recipients: rs,
			});
		},
		SetupKeys: function() {
			return new Promise((accept, reject) => {
				let $keysButton = document.querySelector('#keys-button');
				let $keysSkip = document.querySelector('#keys-skip');
				let $keysSection = document.querySelector('#keys-section');
				let $ownKey = document.querySelector('#own-key');
				let $ownKeyValidity = document.querySelector('#own-key-validity');

				$keysButton.addEventListener('click', function() {
					accept();
				});
				$keysSkip.addEventListener('click', function() {
					accept();
				});

				$keysSection.classList.remove('hidden');
				$ownKey.addEventListener('input', function() {
					setDebounce(() => {
						Welcome.Validate($ownKey, $ownKeyValidity)
							.then((name) => {
								localStorage.setItem(OWN_KEY, name);
								$keysButton.removeAttribute('disabled');
							})
							.catch(() => {
								$keysButton.setAttribute('disabled', '');
							});
					}, 150);
				});
			});
		},
		Validate: function($ownKey, $ownKeyValidity) {
			$ownKey.classList.remove('valid', 'invalid');
			$ownKeyValidity.classList.remove('valid', 'invalid');
			return new Promise((accept, reject) => {
				Welcome.SendValidate([$ownKey.value])
					.then(res => {
						if (res.Errors[0] == '') {
							$ownKey.classList.add('valid');
							$ownKeyValidity.classList.add('valid');
							$ownKeyValidity.title = `${res.Names[0]} (${res.Keys[0]})`;
						} else {
							$ownKey.classList.add('invalid');
							$ownKeyValidity.classList.add('invalid');
							$ownKeyValidity.title = 'Error: ' + res.Errors[0].replace(/\:/g, ':\n');
						}

						accept(res.Names[0]);
					})
					.catch(err => {
						$ownKey.classList.add('invalid');
						$ownKeyValidity.classList.add('invalid');
						$ownKeyValidity.title = 'Error: ' + err.message;
						reject(err);
					});
			});
		},
	};

	return Welcome;
})();

Welcome.Init();
