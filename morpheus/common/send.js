/**
 * Communication across Morpheus modules.
 * */

const Send = (function() {
	let Send = {
		Automatic: function(b) {
			return this.Message({ command: 'automatic', automatic: b });
		},
		Decrypt: function(cipher) {
			return browser.runtime.sendMessage({
				command: 'decrypt',
				cipher: cipher,
			});
		},
		Encrypt: function(message) {
			return browser.runtime.sendMessage({
				command: 'encrypt',
				message: message,
			});
		},
		Inject: function() {
			return browser.runtime.sendMessage({
				command: 'inject',
				state: Morpheus.State(),
			});
		},
		Error: function(err) {
			if (typeof(err) === 'string') {
				err = { message: err };
			}

			this.Message({
				command: 'error',
				error: err,
			});
		},
		Message: function(msg) {
			return browser.runtime.sendMessage(msg);
		},
		GetState: function() {
			return this.Message({ command: 'state' });
		},
		RecipientsNotified: function(s) {
			return this.Message({ command: 'recipients-notified', recipients: s });
		},
		Tab: function(msg) {
			return new Promise((accept, reject) => {
				browser.tabs.query({
					active: true,
					currentWindow: true,
				})
					.then((tabs) => {
						browser.tabs.sendMessage(tabs[0].id, msg);
						accept();
					})
					.catch((err) => {
						this.Error(err);
						reject(err);
					});
			});
		},
		TabExec: function(scr) {
			return new Promise((accept, reject) => {
				browser.tabs.query({
					active: true,
					currentWindow: true,
				})
					.then((tabs) => {
						browser.tabs.executeScript(tabs[0].id, scr);
						accept();
					})
					.catch((err) => {
						this.Error(err);
						reject(err);
					});
			});
		},
	};

	return Send;
})();
