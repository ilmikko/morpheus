/**
 * Wraps around XMLHttpRequest as a promise, supporting both GET and POST (json).
 * */

const HTTP = (function() {
	let HTTP = {
		Port: ICELOS_PORT,
		Protocol: ICELOS_PROTOCOL,
		Method: function(m, url, data) {
			url = HTTP.Protocol + "://localhost:" + HTTP.Port + url;
			return new Promise((accept, reject) => {
				var xhr = new XMLHttpRequest();

				xhr.onreadystatechange = function() {
					if (xhr.readyState < 4) return;
					if (200 <= xhr.status && xhr.status < 400) {
						return accept(xhr.responseText);
					}
					// TODO: new Error() instead of {message}
					if (xhr.status == 0) {
						return reject({
							message: `Connection refused to Icelos on port ${HTTP.Port}.`,
						});
					}
					return reject({
						message: `HTTP ${xhr.status}: ${xhr.responseText}`,
					});
				}
				xhr.onerror = reject;
				xhr.open(m, url);

				if (data == null) {
					xhr.send();
				} else {
					xhr.send(JSON.stringify(data));
				}
			});
		},
		Get: function(url) {
			return this.Method("GET", url);
		},
		Post: function(url, data) {
			return this.Method("POST", url, data);
		},
	};

	return HTTP;
})();
