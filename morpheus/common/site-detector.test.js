function test_no_detection() {
	let got = SiteDetector.Detect("https://some.arbitrary.site/messenger.com");
	let want = '';

	if (got != want) {
		throw new Error(`Unexpected detection:\ngot : ${got}\nwant: ${want}`);
	}
}

function test_detect_morpheus_test() {
	let got = SiteDetector.Detect("file:///home/user/morpheus-test/index.html");
	let want = 'morpheus-test';

	if (got != want) {
		throw new Error(`Unexpected detection:\ngot : ${got}\nwant: ${want}`);
	}
}

function test_detect_facebook_messenger() {
	let got = SiteDetector.Detect("https://www.messenger.com/t/test.user.92");
	let want = 'facebook-messenger';

	if (got != want) {
		throw new Error(`Unexpected detection:\ngot : ${got}\nwant: ${want}`);
	}
}
