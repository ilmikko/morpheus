/**
 * Common conversions Morpheus needs to perform.
 * Detects and converts ciphertext.
 * */

const Convert = (function(){
	function isValidStartToken(s, tok) {
		return s.substr(0, tok.length) == tok;
	}

	function isValidEndToken(s, tok) {
		return s.substr(s.length - tok.length) == tok;
	}

	var Convert = {
		IsCipher: function(text) {
			text = text.trim();
			return this.IsMor(text) || this.IsPGP(text);
		},
		IsCipherStart: function(text) {
			text = text.trim();
			return this.IsMorStart(text) || this.IsPGPStart(text);
		},
		IsCipherEnd: function(text) {
			text = text.trim();
			return this.IsMorEnd(text) || this.IsPGPEnd(text);
		},
		IsMor: function(cipher) {
			return this.IsMorStart(cipher) && this.IsMorEnd(cipher);
		},
		IsMorStart: function(cipher) {
			return isValidStartToken(cipher, MORPHEUS_TOKEN_START);
		},
		IsMorEnd: function(cipher) {
			return isValidEndToken(cipher, MORPHEUS_TOKEN_END);
		},
		IsPGP: function(cipher) {
			return this.IsPGPStart(cipher) && this.IsPGPEnd(cipher);
		},
		IsPGPStart: function(cipher) {
			return isValidStartToken(cipher, PGP_TOKEN_START);
		},
		IsPGPEnd: function(cipher) {
			return isValidEndToken(cipher, PGP_TOKEN_END);
		},
		MorToPGP: function(cipher) {
			cipher = cipher.trim();
			if (!this.IsMor(cipher)) return cipher;
			cipher = cipher.substr(MORPHEUS_TOKEN_START.length);
			cipher = cipher.substr(0, cipher.length - MORPHEUS_TOKEN_END.length);
			cipher = PGP_TOKEN_START + '\n\n' + cipher + '\n' + PGP_TOKEN_END;
			return cipher;
		},
		PGPToMor: function(cipher) {
			cipher = cipher.trim();
			if (!this.IsPGP(cipher)) return cipher;
			cipher = cipher.replace(PGP_TOKEN_START + '\n\n', '');
			cipher = cipher.replace(PGP_TOKEN_END, '');
			cipher = MORPHEUS_TOKEN_START + cipher + MORPHEUS_TOKEN_END;
			return cipher;
		},
	};

	return Convert;
})();
