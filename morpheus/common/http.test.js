const ICELOS_PORT = 0;

// Mock XMLHttpRequest for unit testing purposes.
const XMLHttpRequest = (function() {
	let mockMethods = {
		'http://localhost:0/echo': function() {
			this.status = 200;
			this.responseText = `Echo: ${this._method} ${this._url} ${this.status}`;
		},
		'http://localhost:0/error_server': function() {
			this.status = 500;
			this.responseText = 'Bogus server error';
		},
		'http://localhost:0/error_user': function() {
			this.status = 400;
			this.responseText = 'Bogus user error';
		},
		'http://localhost:0/error_connection': function() {
			this.status = 0;
			this.responseText = '';
		},
	};
	let XMLHttpRequestMock = {
		readyState: 0,
		status: 0,
		responseText: null,
		onreadystatechange: function() {},
		onerror: function() {},
		open: function(method, url) {
			this._method = method;
			this._url = url;
			this._mock = mockMethods[url] || function(){};
		},
		send: function(data) {
			for (let i = 1; i < 4; i++) {
				this.readyState = i;
				this.onreadystatechange();
			}

			this.readyState = 4;
			this.responseText = "404 Bogus not found";
			this.status = 404;
			this._mock();
			this.onreadystatechange();
		},
	};
	return XMLHttpRequestMock;
});

async function test_get_post() {
	HTTP.Get("/echo")
		.then(got => {
			let want = "Echo: GET http://localhost:0/echo 200";
			if (got != want) {
				throw new Error(`/echo unexpected:\ngot : ${got}\nwant: ${want}`);
			}
		});

	HTTP.Post("/echo")
		.then(got => {
			let want = "Echo: POST http://localhost:0/echo 200";
			if (got != want) {
				throw new Error(`/echo unexpected:\ngot : ${got}\nwant: ${want}`);
			}
		});
}

async function test_http_error() {
	HTTP.Get("/error_user")
		.then(got => {
			throw new Error(`/error_user should fail but succeeded, got: ${got}`);
		})
		.catch(err => {
			let want = "HTTP 400: Bogus user error";
			let got = err.message;
			if (got != want) {
				throw new Error(`/error_user message unexpected:\ngot : ${got}\nwant: ${want}`);
			}
		});

	HTTP.Get("/error_server")
		.then(got => {
			throw new Error(`/error_server should fail but succeeded, got: ${got}`);
		})
		.catch(err => {
			let want = "HTTP 500: Bogus server error";
			let got = err.message;
			if (got != want) {
				throw new Error(`/error_server message unexpected:\ngot : ${got}\nwant: ${want}`);
			}
		});
}

async function test_http_error_connection() {
	HTTP.Get("/error_connection")
		.then(got => {
			throw new Error(`/error_connection should fail but succeeded, got: ${got}`);
		})
		.catch(err => {
			let want = "Connection refused to Icelos on port 0.";
			let got = err.message;
			if (got != want) {
				throw new Error(`/error_connection message unexpected:\ngot : ${got}\nwant: ${want}`);
			}
		});
}
