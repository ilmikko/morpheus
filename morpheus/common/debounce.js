var setDebounce = (function() {
	var DEBOUNCES = {};
	return function(f, wait) {
		var self = this;
		var args = arguments;
		if (DEBOUNCES[f]) {
			clearTimeout(DEBOUNCES[f]);
		}
		DEBOUNCES[f] = setTimeout(function() {
			delete DEBOUNCES[f];
			f.apply(self, args);
		}, wait);
	}
})();
