/**
 * Any temporary polyfills for different browser environments.
 * */

var browser = browser || chrome;

// Turns out Firefox also has 'chrome' defined, and navigator is nearly identical,
// so this makes it extremely difficult to determine whether we are running on Firefox or not.
// TODO: Instead of "Chrome", change this to "Not Firefox"

// Chrome
if (typeof navigator !== 'undefined' && navigator && navigator.vendor == 'Google Inc.') {
	if (browser.tabs) {
		// Polyfill executeScript to return a Promise on Chrome.
		browser.tabs.executeScript = (function(original) {
			return function() {
				var args = arguments;
				var self = this;
				return new Promise((accept, reject) => {
					try{
						original.apply(self, args);
						accept();
					}
					catch(err) {
						reject(err);
					}
				});
			}
		})(browser.tabs.executeScript);
		// Polyfill for query to use Promises instead of callbacks.
		browser.tabs.query = (function(original) {
			return function(queryInfo) {
				var args = arguments;
				var self = this;
				return new Promise((accept, reject) => {
					try{
						original.call(self, queryInfo, accept);
					}
					catch(err) {
						reject(err);
					}
				});
			}
		})(browser.tabs.query);
	}
	if (browser.runtime) {
		// Polyfill for onMessage to eat JSON instead of callbacks.
		browser.runtime.onMessage.addListener = (function(original) {
			return function(cbk) {
				var args = arguments;
				var self = this;
				return original.call(self, function(m, s, r) {
					cbk(m, s, function(promise) {
						promise
							.then(res => {
								r({
									error: false,
									original: res,
								});
							})
							.catch(err => {
								r({
									error: true,
									original: err,
								});
							});
					});
					return true; // Indicate we're asynchronous.
				});
			}
		})(browser.runtime.onMessage.addListener);
		// Polyfill for sendMessage to use Promises instead of callbacks.
		browser.runtime.sendMessage = (function(original) {
			return function(queryInfo) {
				var args = arguments;
				var self = this;
				return new Promise((accept, reject) => {
					try{
						original.call(self, queryInfo, (res) => {
							if (res.error) {
								return reject(res.original);
							} else {
								return accept(res.original);
							}
						});
					}
					catch(err) {
						reject(err);
					}
				});
			}
		})(browser.runtime.sendMessage);
	}
}
