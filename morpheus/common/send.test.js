const browser = (function() {
	let browserMock = {
		runtime: {
			sendMessage: function(msg) {
				this._lastMessage = msg;
			},
		},
		tabs: {
			query: function() {
				return new Promise((accept, reject) => {
					accept([{id: "a"}, {id: "b"}]);
				});
			},
			sendMessage: function(id, msg) {
				this._lastMessage = msg;
			},
		},
	};
	return browserMock;
})();

async function test_send_error() {
	{
		Send.Error("Some error happened")

		let got = browser.runtime._lastMessage.error.message;
		let want = 'Some error happened';

		if (got != want) {
			throw new Error(`Unexpected last runtime message:\ngot : ${got}\nwant: ${want}`);
		}
	}

	{
		let got = browser.runtime._lastMessage.command;
		let want = 'error';

		if (got != want) {
			throw new Error(`Unexpected last runtime command:\ngot : ${got}\nwant: ${want}`);
		}
	}

	{
		Send.Error(new Error("Another error happened"))

		let got = browser.runtime._lastMessage.error.message;
		let want = 'Another error happened';

		if (got != want) {
			throw new Error(`Unexpected last runtime message:\ngot : ${got}\nwant: ${want}`);
		}
	}

	{
		let got = browser.runtime._lastMessage.command;
		let want = 'error';

		if (got != want) {
			throw new Error(`Unexpected last runtime command:\ngot : ${got}\nwant: ${want}`);
		}
	}
}

async function test_send_state() {
	Send.GetState({ arbitrary: "state" });

	let got = JSON.stringify(browser.runtime._lastMessage);
	let want = '{"command":"state"}';

	if (got != want) {
		throw new Error(`Unexpected last runtime state:\ngot : ${got}\nwant: ${want}`);
	}
}
