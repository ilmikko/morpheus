/**
 * Detects any sites that have additional Morpheus modules.
 * */

const SiteDetector = (function() {
	let SiteDetector = {
		Current: function() {
			return this.Detect(window.location.href);
		},
		Detect: function(url) {
			if (/web\.whatsapp\.com\/.*/.test(url)) {
				return 'whatsapp-web';
			}
			if (/web\.telegram\.org\/.*/.test(url)) {
				return 'telegram-web';
			}
			if (/app\.slack\.com\/.*/.test(url)) {
				return 'slack';
			}
			// TODO: Differentiate between group chat and single chat (for recipient hints)
			if (/discordapp.com\/.*/.test(url)) {
				return 'discord';
			}
			if (/www\.messenger\.com\/.*/.test(url)) {
				return 'facebook-messenger';
			}
			if (/morpheus\-test\/index.html/.test(url)) {
				return 'morpheus-test';
			}
			if (/^http:\/\/localhost:8000\/.*/.test(url)) {
				return 'morpheus-test';
			}

			// TODO: Enable element picker when site was not detected.
			return '';
		},
	};

	return SiteDetector;
})();
