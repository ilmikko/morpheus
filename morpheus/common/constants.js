/**
 * Defines constants across different modules.
 * */

const ICELOS_PORT = 1234;
const ICELOS_PROTOCOL = 'http';

const HELP_PAGE = 'https://5x.fi/morpheus/help';

const ADVANCED_MODE = 'advanced-mode';
const AUTO_DECRYPT = 'auto-decrypt';
const HELP_MESSAGES = 'help-messages';
const OWN_KEY = 'own-key';
const RECIPIENT_MAP = 'recipient-'
const WELCOME_SHOWN = 'welcome-shown';

const AUTO_DECRYPT_DEFAULT = 'true';
const ADVANCED_MODE_DEFAULT = 'false';
const HELP_MESSAGES_DEFAULT = 'true';

// TODO: Make these editable (esp. if people want to copy paste to PGP)
const MORPHEUS_TOKEN_START = 'Encrypted:[';
const MORPHEUS_TOKEN_END = ']';

const PGP_TOKEN_START = '-----BEGIN PGP MESSAGE-----';
const PGP_TOKEN_END = '-----END PGP MESSAGE-----';
