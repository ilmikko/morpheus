package icelos

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type KeysRequest struct {
	Recipients []string
}

type KeysResponse struct {
	// Errors contains any errors during validation.
	Errors []string
	// Recipients contains the successful recipients for validation.
	Recipients []string
	// Names contains the full names of the associated keys.
	Names []string
	// Keys contains a list of key IDs attached to the names.
	Keys []string
}

func (kr *KeysResponse) String() (string, error) {
	b, err := json.Marshal(kr)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func (i *Icelos) Keys(kq *KeysRequest) (*KeysResponse, error) {
	kr := &KeysResponse{}
	kr.Errors = []string{}
	kr.Keys = []string{}
	kr.Names = []string{}
	kr.Recipients = []string{}

	if len(kq.Recipients) == 0 {
		return nil, fmt.Errorf("No recipients provided.")
	}

	success := false

	for _, r := range kq.Recipients {
		keys, err := i.gpg.GetKeys(r)
		if err != nil {
			log.Printf("GetKeys for %q: %v", r, err)
			kr.Errors = append(kr.Errors, err.Error())
			kr.Keys = append(kr.Keys, "")
			kr.Names = append(kr.Names, "")
			kr.Recipients = append(kr.Recipients, r)
			continue
		}

		for _, k := range keys {
			success = true
			kr.Errors = append(kr.Errors, "")
			kr.Keys = append(kr.Keys, k.PubKey)
			kr.Names = append(kr.Names, k.Name)
			kr.Recipients = append(kr.Recipients, r)
		}
	}

	if !success {
		return nil, fmt.Errorf("Error(s) fetching keys: %s", strings.Join(kr.Errors, "\n"))
	}

	return kr, nil
}

func (i *Icelos) keysHandlerErr(w http.ResponseWriter, r *http.Request) (string, error) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	var kq *KeysRequest
	if err := json.NewDecoder(r.Body).Decode(&kq); err != nil {
		return "", fmt.Errorf("Decode: %v", err)
	}

	kr, err := i.Keys(kq)
	if err != nil {
		return "", fmt.Errorf("Keys: %v", err)
	}

	log.Printf("Fetched %d key(s) for %q", len(kr.Keys), kr.Recipients)
	return kr.String()
}

func (i *Icelos) KeysHandler(w http.ResponseWriter, r *http.Request) {
	out, err := i.keysHandlerErr(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	fmt.Fprintf(w, "%s", out)
}
