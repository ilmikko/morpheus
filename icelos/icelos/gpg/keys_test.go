package gpg_test

import (
	"strings"
	"testing"

	"gitlab.com/ilmikko/morpheus/icelos/icelos/gpg"
)

func TestValidTrust(t *testing.T) {
	testCases := []struct {
		trust string
		want  bool
	}{
		{
			trust: "-", // Unknown (trust is unset by user). We can still use this key for encryption.
			want:  true,
		},
		{
			trust: "q", // Undefined. Almost identical to Unknown.
			want:  true,
		},
		{
			trust: "m", // Marginal. Can be used for encryption.
			want:  true,
		},
		{
			trust: "f", // Full. Can be used for encryption.
			want:  true,
		},
		{
			trust: "u", // Ultimate. Can be used for encryption.
			want:  true,
		},
		{
			trust: "e", // Expired. Do not use.
			want:  false,
		},
		{
			trust: "r", // Revoked. Do not use.
			want:  false,
		},
		{
			trust: "o", // Unknown (new key). Do not use.
			want:  false,
		},
		{
			trust: "i", // Invalid. Do not use.
			want:  false,
		},
		{
			trust: "d", // Disabled by user. Do not use.
			want:  false,
		},
		{
			trust: "n", // No trust (explicit distrust). Do not use.
			want:  false,
		},
	}
	for _, tc := range testCases {
		got := gpg.ValidTrust(tc.trust)
		if want := tc.want; got != want {
			t.Errorf("Trust validity for %q not as expected:\n got: %v\nwant: %v", tc.trust, got, want)
		}
	}
}

func TestVerifyDisambiguity(t *testing.T) {
	fps := gpg.NewFingerprints()

	testCases := []struct {
		fps     map[string]string
		wantErr string
	}{
		{
			fps: map[string]string{},
		},
		{
			fps: map[string]string{
				"123ABC": "Person 1 (Comment) <person1@smt.tld>",
			},
		},
		{
			fps: map[string]string{
				"123ABC": "Person 1 (Comment) <person1@smt.tld>",
				"456DEF": "Person 2 (Comment) <person2@smt.tld>",
			},
			wantErr: "Fingerprints are in conflict for",
		},
		{
			fps: map[string]string{
				"123ABC": "Person 1 (Comment) <person1@smt.tld>",
				"890GHJ": "Person 1 (Comment) <person1@smt.tld>",
			},
		},
		{
			fps: map[string]string{
				"123ABC": "Person 1 (Comment) <person1@smt.tld>",
				"890GHJ": "Person 1 (Other) <person1@smt.tld>",
			},
		},
		{
			fps: map[string]string{
				"123ABC": "Person 1 (Comment) <person1@smt.tld>",
				"890GHJ": "Person 1 (Comment) <other.person1@other.tld>",
			},
		},
	}

	for _, tc := range testCases {
		fps.Fps = tc.fps

		got := fps.VerifyDisambiguity()
		if want := tc.wantErr; want == "" {
			if got != nil {
				t.Errorf("Disambiguity check failed unexpectedly:\n got: %v\nwant: %v", got, want)
			}
		} else {
			if got == nil {
				t.Errorf("Disambiguity check did not fail:\n got: %v\nwant: %v", got, want)
			} else {
				if got := got.Error(); !strings.Contains(got, want) {
					t.Errorf("Disambiguity check returned unexpected error:\n got: %q\nwant: %q", got, want)
				}
			}
		}
	}
}
