package gpg

import (
	"bytes"
	"fmt"
	"os/exec"
)

func RecipientString(ks []*Key) []string {
	s := []string{}

	for _, k := range ks {
		// TODO: use --hidden-recipient?
		s = append(s, "--recipient", k.PubKey)
	}

	return s
}

type GPG struct{}

func (g *GPG) Decrypt(data string) (string, error) {
	cmd := exec.Command(
		"gpg",
		"--batch",
		"--decrypt",
	)

	cmd.Stdin = bytes.NewBuffer([]byte(data))

	o := &bytes.Buffer{}
	e := &bytes.Buffer{}
	cmd.Stdout = o
	cmd.Stderr = e

	if err := cmd.Run(); err != nil {
		return "", fmt.Errorf("run: %v, %s", err, e.String())
	}

	return o.String(), nil
}

func (g *GPG) Encrypt(data []byte, ks []*Key) (string, error) {
	if len(ks) == 0 {
		return "", fmt.Errorf("No keys provided to encrypt for!")
	}

	cmd := exec.Command(
		"gpg",
		append(
			[]string{
				"--batch",
				"--always-trust",
				"--encrypt",
				"--armor",
			},
			RecipientString(ks)...,
		)...,
	)

	cmd.Stdin = bytes.NewBuffer(data)

	o := &bytes.Buffer{}
	e := &bytes.Buffer{}
	cmd.Stdout = o
	cmd.Stderr = e

	if err := cmd.Run(); err != nil {
		return "", fmt.Errorf("run: %v, %s", err, e.String())
	}

	return o.String(), nil
}

func New() *GPG {
	g := &GPG{}
	return g
}
