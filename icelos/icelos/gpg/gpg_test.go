package gpg_test

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/ilmikko/morpheus/icelos/icelos/gpg"
)

func TestEncrypt(t *testing.T) {
	testCases := []struct {
		data []byte
		want string
	}{
		{
			data: []byte("Encrypt this string"),
			want: "-----BEGIN PGP MESSAGE-----",
		},
	}

	g := gpg.New()
	ks, err := g.GetKeys("5")
	if err != nil {
		t.Fatalf("gpg: %v", err)
	}

	for _, tc := range testCases {
		got, err := g.Encrypt(tc.data, ks)
		if err != nil {
			continue
		}
		if want := tc.want; !strings.HasPrefix(got, want) {
			t.Errorf("Encrypt not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestDecrypt(t *testing.T) {
	testCases := []struct {
		data string
		want string
	}{
		{
			data: "-----BEGIN PGP MESSAGE-----\nAAAAA\n-----END PGP MESSAGE-----",
			want: "?",
		},
	}

	g := gpg.New()

	for _, tc := range testCases {
		got, err := g.Decrypt(tc.data)
		if err != nil {
			continue
		}
		if want := tc.want; got != want {
			t.Errorf("Decrypt not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestRecipientString(t *testing.T) {
	testCases := []struct {
		keys []*gpg.Key
		want []string
	}{
		{
			keys: []*gpg.Key{},
			want: []string{},
		},
		{
			keys: []*gpg.Key{
				{
					Name:   "Person 1",
					PubKey: "123ABC",
				},
			},
			want: []string{
				"--recipient",
				"123ABC",
			},
		},
		{
			keys: []*gpg.Key{
				{
					Name:   "Person 1",
					PubKey: "123ABC",
				},
				{
					Name:   "Person 1",
					PubKey: "456DEF",
				},
			},
			want: []string{
				"--recipient",
				"123ABC",
				"--recipient",
				"456DEF",
			},
		},
		{
			keys: []*gpg.Key{
				{
					Name:   "Person 1",
					PubKey: "123ABC",
				},
				{
					Name:   "Person 2",
					PubKey: "789GHJ",
				},
			},
			want: []string{
				"--recipient",
				"123ABC",
				"--recipient",
				"789GHJ",
			},
		},
	}
	for _, tc := range testCases {
		got := gpg.RecipientString(tc.keys)

		if want := tc.want; !reflect.DeepEqual(got, want) {
			t.Errorf("Recipient output not as expected:\n got: %q\n want: %q", got, want)
		}
	}
}
