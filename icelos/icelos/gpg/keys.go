package gpg

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
)

func ValidTrust(trust string) bool {
	switch trust {
	case "-": // Unknown
		return true
	case "q": // Undefined
		return true
	case "m": // Marginal
		return true
	case "f": // Full
		return true
	case "u": // Ultimate
		return true
	case "e": // Expired
		return false
	case "r": // Revoked
		return false
	case "o": // Unknown
		return false
	case "i": // Invalid
		return false
	case "d": // Disabled
		return false
	case "n": // No trust
		return false
	default:
		log.Printf("Warning: Unknown trust %q in format output, skipping...", trust)
		return false
	}
}

type Key struct {
	PubKey string
	Name   string
}

func NewKey(pk string) *Key {
	k := &Key{
		PubKey: pk,
	}
	return k
}

type Fingerprints struct {
	Fps map[string]string
}

func (fps *Fingerprints) Add(fp, name string) {
	fps.Fps[fp] = name
}

func (fps *Fingerprints) VerifyDisambiguity() error {
	prevName := ""
	prevFp := ""
	for fp, fullName := range fps.Fps {
		// If the set of these keys contains people with different names, the set is ambiguous.
		// We do not look at comments or emails, as these vary from person to person.
		// If there truly are two people with the same first and last name in the keychain,
		// the user should be prompted to use direct key fingerprints instead.

		name := fullName

		// Remove e-mail
		name = strings.SplitN(name, "<", 2)[0]

		// Remove comment
		name = strings.SplitN(name, "(", 2)[0]

		// Trim whitespace
		name = strings.Trim(name, " ")

		if prevName != "" && prevName != name {
			return fmt.Errorf("Fingerprints are in conflict for %q (%q) and %q (%q)!",
				prevName,
				prevFp,
				name,
				fp,
			)
		}
		prevName = name
		prevFp = fp
	}
	return nil
}

func NewFingerprints() *Fingerprints {
	return &Fingerprints{
		Fps: map[string]string{},
	}
}

func (g *GPG) getFingerprints(name string) (*Fingerprints, error) {
	fps := NewFingerprints()

	cmd := exec.Command(
		"gpg",
		"--batch",
		"--with-colons",
		"--list-keys",
		name,
	)

	o := &bytes.Buffer{}
	e := &bytes.Buffer{}
	cmd.Stdout = o
	cmd.Stderr = e

	if err := cmd.Run(); err != nil {
		return fps, fmt.Errorf("run: %v, %s", err, e.String())
	}

	s := bufio.NewScanner(o)
	fp := ""

	for s.Scan() {
		fields := strings.Split(s.Text(), ":")
		switch format := fields[0]; format {
		case "tru":
		case "pub":
		case "sub":
		case "fpr":
			fp = fields[9]
		case "uid":
			name := fields[9]
			if trust := fields[1]; ValidTrust(trust) {
				fps.Add(fp, name)
			}
		default:
			log.Printf("Warning: Unknown format %q in GPG output, skipping...", format)
		}
	}

	return fps, nil
}

func (g *GPG) GetKeys(name string) ([]*Key, error) {
	if name == "" {
		return nil, fmt.Errorf("Recipient is empty")
	}
	fps, err := g.getFingerprints(name)
	if err != nil {
		return nil, fmt.Errorf("getFingerprints for %q: %v", name, err)
	}

	if err := fps.VerifyDisambiguity(); err != nil {
		return nil, fmt.Errorf("Keys are ambiguous for %q: %v", name, err)
	}

	if len(fps.Fps) == 0 {
		return nil, fmt.Errorf("No valid keys found for %q", name)
	}

	ks := []*Key{}

	for fp, name := range fps.Fps {
		k := NewKey(fp)
		k.Name = name
		ks = append(ks, k)
	}

	return ks, nil
}
