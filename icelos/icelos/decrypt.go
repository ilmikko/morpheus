package icelos

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type DecryptRequest struct {
	Cipher string
}

func (i *Icelos) Decrypt(r *http.Request) (string, error) {
	var dr DecryptRequest
	if err := json.NewDecoder(r.Body).Decode(&dr); err != nil {
		return "", fmt.Errorf("Decode: %v", err)
	}

	dec, err := i.gpg.Decrypt(dr.Cipher)
	if err != nil {
		return "", fmt.Errorf("Decrypt: %v", err)
	}

	return dec, nil
}

func (i *Icelos) DecryptHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	p, err := i.Decrypt(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	log.Printf("Decrypted message")
	fmt.Fprintf(w, p)
}
