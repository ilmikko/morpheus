package icelos

import (
	"fmt"
	"html"
	"log"
	"net/http"

	"gitlab.com/ilmikko/morpheus/icelos/config"
	"gitlab.com/ilmikko/morpheus/icelos/icelos/gpg"
)

const (
	VERSION = "1.0.0"
)

type Icelos struct {
	cfg *config.Config
	gpg *gpg.GPG
}

func (i *Icelos) Listen() error {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})

	http.HandleFunc("/encrypt", i.EncryptHandler)
	http.HandleFunc("/decrypt", i.DecryptHandler)

	http.HandleFunc("/keys", i.KeysHandler)

	http.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		fmt.Fprintf(w, fmt.Sprintf("Icelos v%s\n", VERSION))
	})

	if i.cfg.TLSEnabled {
		log.Printf("Icelos listening on port %s (TLS)", i.cfg.Port)
		return http.ListenAndServeTLS(
			fmt.Sprintf(":%s", i.cfg.Port),
			fmt.Sprintf("%s", i.cfg.TLSCert),
			fmt.Sprintf("%s", i.cfg.TLSKey),
			nil,
		)
	} else {
		log.Printf("Icelos listening on port %s", i.cfg.Port)
		return http.ListenAndServe(
			fmt.Sprintf(":%s", i.cfg.Port),
			nil,
		)
	}
}

func New(cfg *config.Config) *Icelos {
	return &Icelos{
		cfg: cfg,
		gpg: gpg.New(),
	}
}
