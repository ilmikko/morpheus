package icelos

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/ilmikko/morpheus/icelos/icelos/gpg"
)

type EncryptRequest struct {
	Message string
	Keys    []string
}

func (i *Icelos) Encrypt(r *http.Request) (string, error) {
	var er EncryptRequest
	if err := json.NewDecoder(r.Body).Decode(&er); err != nil {
		return "", fmt.Errorf("Decode: %v", err)
	}

	ks := []*gpg.Key{}
	for _, k := range er.Keys {
		ks = append(ks, gpg.NewKey(k))
	}

	enc, err := i.gpg.Encrypt([]byte(er.Message), ks)
	if err != nil {
		return "", fmt.Errorf("Encrypt: %v", err)
	}

	log.Printf("Encrypted message for keys: %q", er.Keys)
	return enc, nil
}

func (i *Icelos) EncryptHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	enc, err := i.Encrypt(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	fmt.Fprintf(w, enc)
}
