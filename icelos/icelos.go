package main

import (
	"log"

	"gitlab.com/ilmikko/morpheus/icelos/config"
	"gitlab.com/ilmikko/morpheus/icelos/icelos"
)

func mainErr() error {
	i := icelos.New(config.New())
	return i.Listen()
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("ERROR: %v", err)
	}
}
