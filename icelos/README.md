# Icelos

Icelos is the client binary that lives on the host machine and communicates with PGP.
This is to prevent having to run PGP in the browser session, retaining any client PGP configuration for users that already have PGP installed, and to retain private keys in whichever format they may be.
For example, the usage of smart cards via browsers is still highly experimental - Icelos provides an API to use PGP directly via the Morpheus extension.
