package config_test

import (
	"testing"

	"gitlab.com/ilmikko/morpheus/icelos/config"
)

func TestPort(t *testing.T) {
	c := config.New()
	if got, want := c.Port, config.DEFAULT_PORT; got != want {
		t.Errorf("Default port not as expected:\n got: %q\nwant: %q", got, want)
	}
}
