package config

const (
	DEFAULT_PORT        = "1234"
	DEFAULT_TLS_CERT    = "icelos.crt"
	DEFAULT_TLS_KEY     = "icelos.key"
	DEFAULT_TLS_ENABLED = true
)

type Config struct {
	Port       string
	TLSCert    string
	TLSKey     string
	TLSEnabled bool
}

func New() *Config {
	c := &Config{}

	c.Port = DEFAULT_PORT
	c.TLSCert = DEFAULT_TLS_CERT
	c.TLSKey = DEFAULT_TLS_KEY

	return c
}
