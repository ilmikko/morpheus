package main

import (
	"fmt"
	"log"
	"net/http"
)

const (
	PORT = "8000"
)

func main() {
	http.Handle("/", http.FileServer(http.Dir(".")))
	log.Printf("Serving . on port %s", PORT)
	err := http.ListenAndServe(fmt.Sprintf(":%s", PORT), nil)
	log.Fatalf(fmt.Sprintf("server: %v", err))
}
